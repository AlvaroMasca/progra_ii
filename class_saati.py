class Order:
    def __init__(self, id_order: str, customer: str, raw_material: str, min_quality_level: str,
                 min_height: float, max_height: float, min_quantity: float, max_quantity: float, min_cut_length: float,
                 max_cut_length: float):
        """
        :param id_order: Id de la order
        :param customer: Cliente que te ha hecho la orden
        :param raw_material: El material bruto que hay
        :param min_quality_level: El mínimo de calidad aceptado
        :param min_height: Minima altura requerida para llevar a cabo la orden
        :param max_height: Máxima altura requerida para llevar a cabo la orden
        :param min_quantity: Mínimo que se puede producir
        :param max_quantity: Máximo que se puede producir
        :param min_cut_length: Mínimo tamaño de cada articulo
        :param max_cut_length: Maximo tamaño de cada articulo
        """
        self.id_order = id_order
        self.customer = customer
        self.raw_material = raw_material
        self.min_quality_level = min_quality_level
        self.min_height = min_height
        self.max_height = max_height
        self.min_quantity = min_quantity
        self.max_quantity = max_quantity
        self.min_cut_lengt = min_cut_length
        self.max_cut_lenght = max_cut_length

        self.stock_pieces = set([])

    def assign_stock_pieces(self, stocks_piece: 'StockPiece'):
        self.stock_pieces.add(stocks_piece)
        return stocks_piece

    def get_area(self) -> float:
        area = 0
        for sp in self.stock_pieces.values():
            area += sp.length * sp.height
        return area


class StockPiece:

    def __init__(self, id_stock_piece: int, height: float, length: float, quality_level: str,
                 position_x: float, position_y: float, id_father_stock_piece: int):
        """
        :param id_stock_piece:  Id de la pieza en stock
        :param height:  Altura de la pieza
        :param length:  Longitud de la pieza
        :param quality_level: Nivel de calidad de la pieza
        :param position_x:  Coordenadas x empezando desde la esquina superior izquierda
        :param position_y: Coordenadas y empezando desde la esquina superior izquierda
        :param id_father_stock_piece:
        """
        self.id_stock_piece = id_stock_piece
        self.height = height
        self.length = length
        self.quality_level = quality_level
        self.position_x = position_x
        self.position_y = position_y
        self.id_father_stock_piece = id_father_stock_piece

    def assign_order(self, order: "Order"):
        self.order = Order
        order.stock_pieces.append(self)
        return order


class Map:
    def __init__(self, map_type: str, real: bool):
        """
        :param map_type: tipos de mapa: initial,intermediate and final
        :param real: Hace referencia a si el mapa es real o es una estructura esperada
        """
        self.map_type = map_type
        self.real = real

        self.stock_pieces = set([])

    def add_stock_pieces(self, id_stock_piece, height, length, quality_level,
                         position_x, position_y, id_father_stock_piece):
        stocks_pieces = StockPiece(id_stock_piece, height, length, quality_level,
                                   position_x, position_y, id_father_stock_piece)
        self.stock_pieces.add(stocks_pieces)
        return self.stock_pieces


class FabricSheet:
    def __init__(self, id_fabric_sheet: str, raw_material: str, height: float,
                 lenght: float, department: str, weaving_forecast: bool):
        """
        :param id_fabric_sheet: Id de los trozos fabricados
        :param raw_material: Material del que esta compuesto
        :param height:  Altura del torzo
        :param lenght:  Longitud del trozo
        :param department:  Localizacion del trozo favricado
        :param weaving_forecast: Si el trozo es real o pronosticada
        """
        self.id_fabric_sheet = id_fabric_sheet
        self.raw_material = raw_material
        self.height = height
        self.lenght = lenght
        self.department = department
        self.weaving_forecast = weaving_forecast

    def map_initializa(self):
        self.inicial_map = Map("inicial", True)
        self.intermediate_map = Map("Intermediate", True)
        self.final_map = Map("final", True)


# Creacion order
the_order = Order("1", "Alvaro", "cotton", "8", 5, 8, 5, 8, 5, 8)
TheFabricSheet = FabricSheet("1", "cotton", 5, 8, "assembly", True)
AddStockPiece = the_order.assign_stock_pieces

