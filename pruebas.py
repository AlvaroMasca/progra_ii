import sqlite3

conn = sqlite3.connect('users.db')

c = conn.cursor()

c.execute('''CREATE TABLE IF NOT EXISTS Users
             (id int , name text, surname text, email varchar(40), phone_1 int , phone_2 int , age int )''')

c.execute("INSERT INTO Users VALUES (01 ,'Lucas', 'Lopez', 'lucaslopez@gmail.com', 655454333, 912345678, 21)")
c.execute("INSERT INTO Users VALUES (02 ,'Steven', 'Gerard', 'stevenger@gmail.com', 676898010, 908765432, 19)")

conn.commit()

conn.close()





