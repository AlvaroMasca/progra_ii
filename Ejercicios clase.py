import numpy as np
import seaborn as sns
import pandas as pd
from scipy.stats import uniform

price = 140
unit_cost = 5
N = 100000

d_min = 50
d_max = 80

# optimal based on formula q_opt = F-1((price-unit_cost)/price)
q_opt = d_min + (d_max - d_min) * uniform.ppf((price-unit_cost)/price)

demand = np.random.uniform(low=d_min, high=d_max, size=N)

print(np.mean(demand))
print(np.std(demand))
# sns.histplot(data=demand, binwidth=0.1, stat="probability")

print(demand[0:10])

results = {}

for q in range(45,70):

    # Expected revenue
    revenue = price * np.minimum(demand, q)
    cost = unit_cost * q
    profit = float(np.mean(revenue - cost))

    results.update({q: round(profit, 2)})

result_table = pd.DataFrame.from_dict(results,
                                      orient='index',
                                      columns=['E_profit']).reset_index().rename(
                                                    columns={'index': 'q'})
print(result_table)

sns.scatterplot(data=result_table, x='q', y='E_profit')