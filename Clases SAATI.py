

class Order:
    def __init__(self, Idorder: str,Customer:str, RawMaterial:str, MinQualityLevel:str,
                 MinHeight: float , MaxHeight: float, MinQuantity:float, MaxQuantity:float, MincutLength:float, MaxCutLenght:float):
        """
        :param Idorder: Id de la orden
        :param Customer: Cliente que te ha hecho la orden
        :param RawMaterial: El material bruto que hay
        :param MinQualityLevel: El mínimo de calidad aceptado
        :param MinHeight: Minima altura requerida para llevar a cabo la orden
        :param MaxHeight: Máxima altura requerida para llevar a cabo la orden
        :param MinQuantity: Mínimo que se puede producir
        :param MaxQuantity: Máximo que se puede producir
        :param MincutLength: Mínimo tamaño de cada articulo
        :param MaxCutLenght: Maximo tamaño de cada articulo
        """
        self.Idorder=Idorder
        self.Customer=Customer
        self.RawMaterial=RawMaterial
        self.MinQualityLevel=MinQualityLevel
        self.MinHeight=MinHeight
        self.MaxHeight=MaxHeight
        self.MinQuantity=MinQuantity
        self.MaxQuantity=MaxQuantity
        self.MincutLength=MincutLength
        self.MaxCutLenght=MaxCutLenght

        self.StockPieces = set([])


    def GetAssignedQuantity(self,MinQuantity,MaxQuantity):


"""
La cantidad cumplida de la order
"""


    def CalcDelay(self,):

"""
El retraso producido de la order
"""

    def add_StockPiece(self, stockpiece: 'StockPiece'):
        self.StockPieces.add(stockpiece)

"""
Método que permite asignarle un elemento de stock a la orden
"""

class StockPiece:
    def __init__(self, IdStockPiece: int, Height: float, Length: float, QualityLevel: str,
                 PositionX: float, PositionY:float, IdFatherStockPiece: int):
        """
        :param IdStockPiece:  Id de la pieza en stock
        :param Height:  Altura de la pieza
        :param Length:  Longitud de la pieza
        :param QualityLevel: Nivel de calidad de la pieza
        :param PositionX:  Coordenadas x empezando desde la esquina superior izquierda
        :param PositionY: Coordenadas y empezando desde la esquina superior izquierda
        :param IdFatherStockPiece:
        """
        self.IdStockPiece= IdStockPiece
        self.Height=Height
        self.Length=Length
        self.QualityLevel=QualityLevel
        self.PositionX=PositionX
        self.PositionY=PositionY
        self.IdFatherStockPiece=IdFatherStockPiece

    def GetArea(self,Height,Lenght):

    def CheckResidual(self,):

    def AssignOrder(self,):

class Map:
    def __init__(self,Maptype: str, Real:bool):
        """
        :param Maptype: tipos de mapa: initial,intermediate and final
        :param Real: Hace referencia a si el mapa es real o es una estructura esperada
        """
        self.Maptype=Maptype
        self.Real=Real

    def GetAssignedQuantity(self):

    def CalcResidualArea(self,):

    def AddStockPiece(self,):

    def DeleteStockPiece(self,):



class FabricSheet:
    def __init__(self, IdFabricSheet:str,RawMaterial:str, Height:float,
                 Lenght:float, Department:str, WeavingForecast:bool):
        """
        :param IdFabricSheet: Id de los trozos fabricados
        :param RawMaterial: Material del que esta compuesto
        :param Height:  Altura del torzo
        :param Lenght:  Longitud del trozo
        :param Department:  Localizacion del trozo favricado
        :param WeavingForecast: Si el trozo es real o pronosticada
        """
        self.IdFabricSheet=IdFabricSheet
        self.RawMaterial=RawMaterial
        self.Height=Height
        self.Lenght=Lenght
        self.Department=Department
        self.WeavingForecast=WeavingForecast

    def SelectMap(self,Maptype):


    def AddMap(self,):
