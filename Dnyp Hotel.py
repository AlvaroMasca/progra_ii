import numpy as np
from itertools import repeat
import json

PRICE = 140
PRICE_2 = 120
N = 500000

dicc = {70: 40, 80: 34, 90: 30, 100: 27, 110: 24, 120: 22, 140: 18, 160: 15}


def sim_demand():
    variable = dicc[PRICE]
    demand = np.maximum(np.random.poisson(variable, size=N), 0)
    print(demand)


sim_demand()


def deterministic_model(q: float, demand: float) -> float:
    revenue = PRICE * min(sim_demand())
    return revenue


def monte_carlo():
    demand = sim_demand()

    results = {}

    for q in range(0, 70):
        # Expected revenue
        profit = map(deterministic_model, repeat(q, N), demand)
        e_profit = sum(profit) / N

        results.update({q: round(e_profit, 2)})

    return results


monte_carlo()

if __name__ == '__main__':
    sim_results = monte_carlo()
    best_q = max(sim_results, key=sim_results.get)
    print(best_q)

    with open('resultados.json', 'w') as f:
        json.dump(sim_results, f, sort_keys=True, indent=4)