import sqlite3
import re
from email_validator import validate_email, EmailNotValidError

conn = sqlite3.connect('users')


class Field:
    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, obj, objtype=None):
        return conn.execute(self.fetch, [obj.key]).fetchone()[0]

    def __set__(self, obj, value):
        conn.execute(self.store, [value, obj.key])
        conn.commit()


class DescriptorEmail:
    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, obj, objtype=None):
        return conn.execute(self.fetch, [obj.key]).fetchone()[0]

    def __set__(self, obj, value):
        try:
            valid = validate_email(obj)

            value = valid.email
            conn.execute(self.store, [value, obj.key])
            conn.commit()
        except EmailNotValidError as e:
            # email is not valid, exception message is human-readable
            print(str(e))


class DescriptorPhones:
    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, obj, objtype=None):
        return conn.execute(self.fetch, [obj.key]).fetchone()[0]

    def __set__(self, obj, value):
        try:
            if bool(re.fullmatch(r"[0-9]{9}", value)) or \
                    bool(re.fullmatch(r"\+[0-9]{10,11}", value)):
                conn.execute(self.store, [value, obj.key])
                conn.commit()
        except:
            raise ValueError


class DescriptorAge:
    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, obj, objtype=None):
        return conn.execute(self.fetch, [obj.key]).fetchone()[0]

    def __set__(self, obj, value):
        try:
            if isinstance(value, int) & value > 0 & value < 120:
                conn.execute(self.store, [value, obj.key])
                conn.commit()
        except:
            raise ValueError


class User:
    table = "users"
    key = "id"
    name = Field()
    surname = Field()
    email = DescriptorEmail()
    phone_1 = DescriptorPhones()
    phone_2 = DescriptorPhones()
    age = DescriptorAge()

    def __init__(self, key):
        self.key = key


bob = User(1)
bob.age = 21
bob.phone_1 = "444444444"

try:
    bob.age = 200
except:
    print('ValueError: inappropiate age value')

try:
    bob.phone_1 = 4444444
except:
    print('ValueError: inappropiate phone value')

try:
    bob.email = "r.vazquez@uf"
except:
    print('ValueError: inappropiate email value')
